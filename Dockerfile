FROM gradle:6.8-jdk11

RUN apt-get update && apt-get install -y bash gcc build-essential zlib1g-dev

WORKDIR /workspace
VOLUME /workspace

ENTRYPOINT ["bash", "-C"]
CMD [ "gradlew", "tasks" ]
